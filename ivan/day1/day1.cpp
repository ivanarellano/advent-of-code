#include <math.h>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "../include/timer.h"
#include "../include/parse.h"
#include "../include/window-util.h"

class Module
{
  public:
    Module(int mass = 0)
      {
        load_fuel(mass);
      };

    int get_fuel() { return fuel; }
  private:
    void load_fuel(int mass);
    int fuel = 0;
    
    // Copy constructor
    Module(const Module&) = delete;
    // Assignment
    Module& operator=(const Module&) = delete;
};

void Module::load_fuel(int mass) {
  int runningFuel = std::floor(mass / 3) - 2;

  if (runningFuel <= 0) return;

  fuel += runningFuel;

  load_fuel(runningFuel);
}

class FuelCounterUpper
{
  public:
    FuelCounterUpper(const std::string& file_name)
    {
      count_modules_from_file(file_name);
    };

    const int get_sum_fuel() { return sumFuel; }
  private:
    void count_modules_from_file(const std::string& file_name);
    int sumFuel;
};

void FuelCounterUpper::count_modules_from_file(const std::string& file_name)
{
  std::ifstream in_stream {file_name};

  if (!in_stream) 
  {
    std::runtime_error { "Can't open file: " + file_name };
    return;
  }

  std::string line;

  while (!in_stream.eof())
  {
    // No delimiter
    getline(in_stream, line);

    sumFuel += Module(str_to_integer(line)).get_fuel();
  }
}

int main()
{
  FuelCounterUpper fcu {"days/1/input_2.txt"};
  std::cout << "Sum of the fuel requirements: " << fcu.get_sum_fuel() << std::endl;

  keep_window_open();

  return 0;
}