#pragma once

#include <iostream>
#include <chrono>

typedef int Solution();

void printSolutionDelta(Solution solution)
{
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

  int output { solution() };

  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

  std::cout << "Solution: " << output
    << std::endl
    << "Time difference in ms = "
    << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count()
    << std::endl;
}