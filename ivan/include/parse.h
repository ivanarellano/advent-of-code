#pragma once

#include <string>
#include <sstream>

int str_to_integer(const std::string& s)
{
  std::istringstream is {s};
  
  int i;
  is >> i;
  
  if (!is)
  {
    std::runtime_error("int format error:" + s);
  }

  return i;
}