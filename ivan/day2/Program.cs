﻿using System;
using System.IO;

namespace day2
{
    class Program
    {
        static void Main(string[] args)
        {
            string readInput()
            {
                try
                {
                    using (StreamReader sr = new StreamReader("input_part1.txt"))
                    {
                        return sr.ReadToEnd();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{e.Message}");
                }

                return string.Empty;
            }

            int parseIntcodes(int[] intcodes)
            {
                for (int i = 0; i < intcodes.Length - 4; i += 4)
                {
                    var pos1 = intcodes[i+1];
                    var pos2 = intcodes[i+2];
                    var pos3 = intcodes[i+3];

                    switch (intcodes[i])
                    {
                        case 1:
                            intcodes[pos3] = intcodes[pos1] + intcodes[pos2];
                        break;
                        case 2:
                            intcodes[pos3] = intcodes[pos1] * intcodes[pos2];
                        break;
                        case 99:
                            i = intcodes.Length;
                        break;
                    }
                }
                
                return intcodes[0];
            }

            void part_1()
            {
                var intcodes = Array.ConvertAll(readInput().Split(','), int.Parse);

                intcodes[1] = 12;
                intcodes[2] = 2;

                parseIntcodes(intcodes);

                Console.WriteLine(intcodes[0]);
            }

            void part_2()
            {
                var intcodes = Array.ConvertAll(readInput().Split(','), int.Parse);

                for (int noun = 0; noun <= 99; noun++)
                {
                    for (int verb = 0; verb <= 99; verb++)
                    {
                        var codesCopy = (int[])intcodes.Clone();

                        codesCopy[1] = noun;
                        codesCopy[2] = verb;

                        if (parseIntcodes(codesCopy) == 19690720)
                        {
                            Console.WriteLine($"{100 * noun + verb}");
                            return;
                        }
                    }
                }
            }

            part_2();
        }
    }
}
